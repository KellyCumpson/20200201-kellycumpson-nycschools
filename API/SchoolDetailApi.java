package com.example.challange_20200201_kellycumpson_nycschools.API;

import com.example.challange_20200201_kellycumpson_nycschools.Model.SchoolDetail;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface SchoolDetailApi {
    @GET
    Call<List<SchoolDetail>> getSchoolDetail(@Url String selectedSchool);
}
