package com.example.challange_20200201_kellycumpson_nycschools.API;

import com.example.challange_20200201_kellycumpson_nycschools.Model.School;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface SchoolDirectoryApi {
    @GET("s3k6-pzi2.json?$select=school_name,dbn")
    Call<List<School>> getSchoolArrayList();

}
