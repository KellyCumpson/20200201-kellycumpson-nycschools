package com.example.challange_20200201_kellycumpson_nycschools.Services;

import android.app.IntentService;
import android.content.Intent;
import com.example.challange_20200201_kellycumpson_nycschools.API.SchoolDirectoryApi;
import com.example.challange_20200201_kellycumpson_nycschools.Model.School;
import com.example.challange_20200201_kellycumpson_nycschools.Model.Schools;
import com.google.gson.Gson;

import java.util.List;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.challange_20200201_kellycumpson_nycschools.Contstants.BASE_URL;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class SchoolsDirectoryService extends IntentService {

    private static final String TAG = SchoolsDirectoryService.class.getSimpleName();
    public static final String PENDING_RESULT_EXTRA = "pending_result";


    public SchoolsDirectoryService() {
        super("SchoolsDirectoryService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            actionGetSchools();
        }
    }

    private void actionGetSchools() {
        final Schools getSchoolArrayListResponse = new Schools();
        final String schoolList;
        final boolean isSuccessful;
        final String errorMessage;

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        SchoolDirectoryApi schoolDirectoryApi = retrofit.create(SchoolDirectoryApi.class);

        Call<List<School>> callSync = schoolDirectoryApi.getSchoolArrayList();

        try {
            Response<List<School>> response = callSync.execute();
            if (!response.isSuccessful()) {
                schoolList = "";
                errorMessage = response.code() + ": " + response.message();
                isSuccessful = false;
            } else {
                Gson gson = new Gson();
                schoolList = gson.toJson(response.body());
                errorMessage = "none";
                isSuccessful = true;
            }
            Intent sendSchools = new Intent();
            sendSchools.setAction("GET_SCHOOLS");
            sendSchools.putExtra( "SCHOOLS", schoolList);
            sendSchools.putExtra("IS_SUCCESS", isSuccessful);
            sendSchools.putExtra("ERROR_MESSAGE", errorMessage);
            sendBroadcast(sendSchools);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }



    @Override
    public void onDestroy(){
        super.onDestroy();
    }

}
