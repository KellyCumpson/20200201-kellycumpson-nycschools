package com.example.challange_20200201_kellycumpson_nycschools.Services;

import android.app.IntentService;
import android.content.Intent;

import com.example.challange_20200201_kellycumpson_nycschools.API.SchoolDetailApi;
import com.example.challange_20200201_kellycumpson_nycschools.Model.SchoolDetail;
import com.example.challange_20200201_kellycumpson_nycschools.Model.School;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.challange_20200201_kellycumpson_nycschools.Contstants.BASE_URL;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class SchoolDetailService extends IntentService {


    private static final String TAG = SchoolDetailService.class.getSimpleName();
    public static final String PENDING_RESULT_EXTRA = "pending_result";


    public SchoolDetailService() {
        super("SchoolDetailService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            actionGetSchoolDetail(intent);
        }
    }

    private void actionGetSchoolDetail(Intent intent) {
        final String schoolDetail;
        final boolean isSuccessful;
        final String errorMessage;
        final String selectedSchoolDbn = intent.getStringExtra("SCHOOL_DBN");
        final String selectSchoolURL = "f9bf-2cp4.json?dbn=" + selectedSchoolDbn;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        SchoolDetailApi schoolDirectoryApi = retrofit.create(SchoolDetailApi.class);

        Call<List<SchoolDetail>> callSync = schoolDirectoryApi.getSchoolDetail(selectSchoolURL);

        try {
            Response<List<SchoolDetail>> response = callSync.execute();
            if (!response.isSuccessful()) {
                schoolDetail = "";
                errorMessage = response.code() + ": " + response.message();
                isSuccessful = false;
            } else {
                Gson gson = new Gson();
                schoolDetail = gson.toJson(response.body());
                errorMessage = "none";
                isSuccessful = true;
            }
            Intent sendSchoolDetails = new Intent();
            sendSchoolDetails.setAction("GET_SCHOOL_DETAIL");
            sendSchoolDetails.putExtra( "SCHOOL_DETAIL", schoolDetail);
            sendSchoolDetails.putExtra("IS_SUCCESS", isSuccessful);
            sendSchoolDetails.putExtra("ERROR_MESSAGE", errorMessage);
            sendBroadcast(sendSchoolDetails);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    public void onDestroy(){
        super.onDestroy();
    }
}
