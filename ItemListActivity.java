package com.example.challange_20200201_kellycumpson_nycschools;

import android.Manifest;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import com.example.challange_20200201_kellycumpson_nycschools.Model.School;
import com.example.challange_20200201_kellycumpson_nycschools.Model.Schools;
import com.example.challange_20200201_kellycumpson_nycschools.Services.SchoolsDirectoryService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


/**
 * An activity representing a list of Items. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link ItemDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class ItemListActivity extends AppCompatActivity {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;
    private List<School> schoolList;
    SchoolsReceiver schoolsReceiver;

    public String activeSchoolName = "School Directory";


    public int REQUEST_AUDIO_PERMISSION_RESULT = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);

        //all versions after marshmallow require you to request access to the user's mic on their device
        //this app does not intentionally access the mic but this permission request will prevent the app from crashing on versions later than marshmallow
        requestRecordAudioPermission();

        schoolsReceiver = new SchoolsReceiver();
        registerReceiver(schoolsReceiver, new IntentFilter("GET_SCHOOLS"));  //<----Register
        PendingIntent pendingResult = createPendingResult(
               0, new Intent(), 0);
        Intent intent = new Intent(getApplicationContext(), SchoolsDirectoryService.class);
        intent.putExtra(SchoolsDirectoryService.PENDING_RESULT_EXTRA, pendingResult);
        startService(intent);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(activeSchoolName);


        if (findViewById(R.id.item_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }

    }


    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerView.setAdapter(new SimpleItemRecyclerViewAdapter(this, schoolList, mTwoPane));
    }

    public static class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

        private final ItemListActivity mParentActivity;
        private final List<School> mSchoolNames;
        private final boolean mTwoPane;
        private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                School school = (School) view.getTag();
                if (mTwoPane) {
                    Bundle arguments = new Bundle();
                    arguments.putString(ItemDetailFragment.ARG_ITEM_ID, school.getDataBaseName());
                    ItemDetailFragment fragment = new ItemDetailFragment();
                    fragment.setArguments(arguments);
                    mParentActivity.getSupportFragmentManager().beginTransaction()
                            .replace(R.id.item_detail_container, fragment)
                            .commit();
                } else {
                    Context context = view.getContext();
                    Intent intent = new Intent(context, ItemDetailActivity.class);
                    intent.putExtra(ItemDetailFragment.ARG_ITEM_ID, school.getDataBaseName());

                    context.startActivity(intent);
                }
            }
        };

        SimpleItemRecyclerViewAdapter(ItemListActivity parent,
                                      List<School> items,
                                      boolean twoPane) {
            mSchoolNames = items;
            mParentActivity = parent;
            mTwoPane = twoPane;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            holder.mIdView.setText(mSchoolNames.get(position).getSchoolName());
           // holder.mContentView.setText(mSchoolNames.get(position).getSchoolName());

            holder.itemView.setTag(mSchoolNames.get(position));
            holder.itemView.setOnClickListener(mOnClickListener);
        }

        @Override
        public int getItemCount() {
            return mSchoolNames.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            final TextView mIdView;
            final TextView mContentView;

            ViewHolder(View view) {
                super(view);
                mIdView = (TextView) view.findViewById(R.id.id_text);
                mContentView = (TextView) view.findViewById(R.id.content);
            }
        }

    }



    public void getSchoolsRunnableComplete(Schools schools){
        if(schools.isCallSuccessful()){
            schoolList = schools.getSchoolArrayList();
        }
            View recyclerView = findViewById(R.id.item_list);
            assert recyclerView != null;
            setupRecyclerView((RecyclerView) recyclerView);
    }


    private void requestRecordAudioPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) ==
                    PackageManager.PERMISSION_GRANTED) {
                // put your code for Version>=Marshmallow
            } else {
                if (shouldShowRequestPermissionRationale(Manifest.permission.RECORD_AUDIO)) {
                    Toast.makeText(this,
                            "App required access to audio", Toast.LENGTH_SHORT).show();
                }
                requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO
                }, REQUEST_AUDIO_PERMISSION_RESULT);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_AUDIO_PERMISSION_RESULT) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getApplicationContext(),
                        "Application will not have audio on record", Toast.LENGTH_SHORT).show();
            }
        }
    }

    class SchoolsReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            if(intent.getAction().equals("GET_SCHOOLS"))
            {
                Gson gson = new Gson();
                List<School> schoolList;
                Schools schools = new Schools();
                String errorMessage = "";
                boolean isSuccessful = false;

                String schoolsJsonString = intent.getStringExtra("SCHOOLS");
                Type type = new TypeToken<ArrayList<School>>(){}.getType();
                schoolList = gson.fromJson(schoolsJsonString, type);

                errorMessage = intent.getStringExtra("ERROR_MESSAGE");

                isSuccessful = intent.getBooleanExtra("IS_SUCCESS", false);

                schools.setSchoolArrayList(schoolList);
                schools.setErrorMessage(errorMessage);
                schools.setCallSuccessful(isSuccessful);

                getSchoolsRunnableComplete(schools);

            }
        }

    }

    @Override
    public void onDestroy(){
        schoolList = null;
        schoolsReceiver = null;
        activeSchoolName = null;
        super.onDestroy();
    }

}
