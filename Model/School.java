package com.example.challange_20200201_kellycumpson_nycschools.Model;

import com.google.gson.annotations.SerializedName;

public class School {

    @SerializedName("school_name")
    private String schoolName;
    @SerializedName("dbn")
    private String dataBaseName;

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getDataBaseName() {
        return dataBaseName;
    }

    public void setDataBaseName(String dataBaseName) {
        this.dataBaseName = dataBaseName;
    }
}


