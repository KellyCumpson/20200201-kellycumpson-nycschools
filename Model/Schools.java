package com.example.challange_20200201_kellycumpson_nycschools.Model;

import java.util.ArrayList;
import java.util.List;


public class Schools {

    private boolean callSuccessful;
    private String errorMessage;
    private List<School> schoolArrayList = new ArrayList<>();

    public List<School> getSchoolArrayList() {

        return schoolArrayList;
    }

    public boolean isCallSuccessful() {
        return callSuccessful;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setSchoolArrayList(List<School> schoolArrayList) {
        this.schoolArrayList = schoolArrayList;
    }


    public void setCallSuccessful(boolean callWasSuccessful) {
        this.callSuccessful = callWasSuccessful;
    }


    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
