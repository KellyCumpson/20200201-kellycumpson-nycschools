package com.example.challange_20200201_kellycumpson_nycschools.Model;

import com.google.gson.annotations.SerializedName;

public class SchoolDetail {

    @SerializedName("dbn")
    private String dataBaseName;
    @SerializedName("school_name")
    private String schoolName;
    @SerializedName("num_of_sat_test_takers")
    private int numberOfTestTakers;
    @SerializedName("sat_critical_reading_avg_score")
    private int satCriticalReadingAverageScore;
    @SerializedName("sat_math_avg_score")
    private int satMathAverageScore;
    @SerializedName("sat_writing_avg_score")
    private int satWritingAverageScore;
    private boolean callSuccessful;
    private String errorMessage;

    public SchoolDetail (String dataBaseName,
                                        String schoolName,
                                        int numberOfTestTakers,
                                        int satCriticalReadingAverageScore,
                                        int satMathAverageScore,
                                        int satWritingAverageScore){
        this.dataBaseName = dataBaseName;
        this.schoolName = schoolName;
        this.numberOfTestTakers = numberOfTestTakers;
        this.satCriticalReadingAverageScore = satCriticalReadingAverageScore;
        this.satMathAverageScore = satMathAverageScore;
        this.satWritingAverageScore = satWritingAverageScore;
    }

    public SchoolDetail (boolean callSuccessful, String errorMessage){
        this.callSuccessful = callSuccessful;
        this.errorMessage = errorMessage;
    }


    public String getDataBaseName() {
        return dataBaseName;
    }

    public void setDataBaseName(String dataBaseName) {
        this.dataBaseName = dataBaseName;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public int getNumberOfTestTakers() {
        return numberOfTestTakers;
    }

    public void setNumberOfTestTakers(int numberOfTestTakers) {
        this.numberOfTestTakers = numberOfTestTakers;
    }

    public int getSatCriticalReadingAverageScore() {
        return satCriticalReadingAverageScore;
    }

    public void setSatCriticalReadingAverageScore(int satCriticalReadingAverageScore) {
        this.satCriticalReadingAverageScore = satCriticalReadingAverageScore;
    }

    public int getSatMathAverageScore() {
        return satMathAverageScore;
    }

    public void setSatMathAverageScore(int satMathAverageScore) {
        this.satMathAverageScore = satMathAverageScore;
    }

    public int getSatWritingAverageScore() {
        return satWritingAverageScore;
    }

    public void setSatWritingAverageScore(int satWritingAverageScore) {
        this.satWritingAverageScore = satWritingAverageScore;
    }

    public boolean isCallSuccessful() {
        return callSuccessful;
    }

    public void setCallSuccessful(boolean callSuccessful) {
        this.callSuccessful = callSuccessful;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
