package com.example.challange_20200201_kellycumpson_nycschools;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import com.example.challange_20200201_kellycumpson_nycschools.Model.SchoolDetail;
import com.example.challange_20200201_kellycumpson_nycschools.Services.SchoolDetailService;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a {@link ItemListActivity}
 * in two-pane mode (on tablets) or a {@link ItemDetailActivity}
 * on handsets.
 */
public class ItemDetailFragment extends Fragment {

    SchoolDetailReceiver schoolDetailReceiver;
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";

    private SchoolDetail schoolDetail;
    private String selectedListItem;
    View rootView;

    TextView _textViewSchoolName;
    TextView _textViewNumberOfTestTakers;
    TextView _textViewSatReadingAverageScore;
    TextView _textViewSatMathAverageScore;
    TextView _textViewSatWritingAverageScore;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        selectedListItem = args.getString(ItemDetailFragment.ARG_ITEM_ID);
        schoolDetailReceiver = new SchoolDetailReceiver();
        getActivity().registerReceiver(schoolDetailReceiver, new IntentFilter("GET_SCHOOL_DETAIL"));  //<----Register
        PendingIntent pendingResult = getActivity().createPendingResult(
                0, new Intent(), 0);
        Intent intent = new Intent(getActivity().getApplicationContext(), SchoolDetailService.class);
        intent.putExtra(SchoolDetailService.PENDING_RESULT_EXTRA, pendingResult);
        intent.putExtra("SCHOOL_DBN", selectedListItem);
        getActivity().startService(intent);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.school_detail, container, false);

        // Show the schoolDetail content as text in a TextView.
        if (schoolDetail != null) {
            try {
                checkIfTextViewsAreNull();
                setTextViews();
            }catch(Exception e){
                e.printStackTrace();
            }
        }

        return rootView;
    }

    class SchoolDetailReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            if(intent.getAction().equals("GET_SCHOOL_DETAIL"))
            {
                Gson gson = new Gson();
                List<SchoolDetail> schoolDetailData;

                String schoolDetailJsonString = intent.getStringExtra("SCHOOL_DETAIL");
                Type type = new TypeToken<List<SchoolDetail>>(){}.getType();
                schoolDetailData = gson.fromJson(schoolDetailJsonString, type);
                schoolDetail = schoolDetailData.get(0);
                renderSchoolDetail();
            }
        }

    }

    public View renderSchoolDetail () {
        if (schoolDetail != null) {
            try {
                checkIfTextViewsAreNull();
                setTextViews();
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        return rootView;
    }

    public void checkIfTextViewsAreNull(){
        try {
            if (_textViewSchoolName == null) {
                _textViewSchoolName = new TextView(getActivity().getApplicationContext());
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        try{
        if (_textViewNumberOfTestTakers == null) {
            _textViewNumberOfTestTakers = new TextView(getActivity().getApplicationContext());
        }
        }catch(Exception e){
            e.printStackTrace();
        }
        try{
        if (_textViewSatReadingAverageScore == null) {
            _textViewSatReadingAverageScore = new TextView(getActivity().getApplicationContext());
        }
        }catch(Exception e){
            e.printStackTrace();
        }
        try{
        if (_textViewSatMathAverageScore == null) {
            _textViewSatMathAverageScore = new TextView(getActivity().getApplicationContext());
        }
        }catch(Exception e){
            e.printStackTrace();
        }
        try{
        if (_textViewSatWritingAverageScore == null) {
            _textViewSatWritingAverageScore = new TextView(getActivity().getApplicationContext());
        }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public void setTextViews(){
        try {
            TextView _textViewSchoolName = rootView.findViewById(R.id.schoolNameDetailView);
            _textViewSchoolName.setText(String.valueOf(schoolDetail.getSchoolName()));

            TextView _textViewNumberOfTestTakers = rootView.findViewById(R.id.numberOfTestTakers);
            _textViewNumberOfTestTakers.setText(String.valueOf(schoolDetail.getNumberOfTestTakers()));

            TextView _textViewSatReadingAverageScore = rootView.findViewById(R.id.satAverageReadingScore);
            _textViewSatReadingAverageScore.setText(String.valueOf(schoolDetail.getSatCriticalReadingAverageScore()));

            TextView _textViewSatMathAverageScore = rootView.findViewById(R.id.satAverageMathScore);
            _textViewSatMathAverageScore.setText(String.valueOf(schoolDetail.getSatMathAverageScore()));

            TextView _textViewSatWritingAverageScore = rootView.findViewById(R.id.satAverageWritingScore);
            _textViewSatWritingAverageScore.setText(String.valueOf(schoolDetail.getSatWritingAverageScore()));
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy(){
        _textViewSchoolName = null;
        _textViewNumberOfTestTakers = null;
        _textViewSatReadingAverageScore = null;
        _textViewSatMathAverageScore = null;
        _textViewSatWritingAverageScore = null;
        super.onDestroy();
    }


}
